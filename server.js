if (process.env.NODE_ENV === 'development') require('dotenv').config();

const http = require('http');
const express = require('express');
const twilio = require('twilio');
const bodyParser = require('body-parser');
const axios = require('axios');
const _get = require('lodash/get');
const _isEmpty = require('lodash/isEmpty');

const formatText = require('./lib/format-text');
const generateId = require('./lib/new-id');

const { TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN, TWILIO_MY_NUMBER, QUERY_URL, MY_URL } = process.env;

// Twilio Setup
const accountSid = TWILIO_ACCOUNT_SID;
const authToken = TWILIO_AUTH_TOKEN;
const client = twilio(accountSid, authToken);
const MessagingResponse = twilio.twiml.MessagingResponse;

// create express app
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const requests = new Map();

app.post('/sms', (req, res) => {
    const message = req.body.Body;
    const from = req.body.From;

    const [_action, ...rest] = message.split(' ');
    const action = (_action || '').toLowerCase();
    const param = rest.join(' ');

    switch (action) {
        case 'score':
            const id = generateId();
            requests.set(id, { number: from, action });

            axios
                .post(QUERY_URL, {
                    team: param,
                    endpoint: `${MY_URL}/${id}`
                })
                .catch(er => {
                    const twiml = new MessagingResponse();
                    twiml.message('There was an error fetching the score data');
                    res.writeHead(200, { 'Content-Type': 'text/xml' });
                    res.end(twiml.toString());
                });
            break;
        default:
            const twiml = new MessagingResponse();
            twiml.message(
                'Uh oh, we didn\'t understand that. Send an action and then a team. For example: "score warriors"'
            );
            res.writeHead(200, { 'Content-Type': 'text/xml' });
            res.end(twiml.toString());
    }
});

function sendMessage(number, message) {
    client.messages
        .create({
            to: number,
            from: TWILIO_MY_NUMBER,
            body: message
        })
        .then(message => console.log(message.sid))
        .catch(er => console.error('error', er));
}

app.post('/request_fulfilled/:id', (req, res) => {
    let data = req.body;
    let id = req.params.id;

    console.log('got a fulfilled request', id, data);

    let request = requests.get(id);
    if (request) {
        let { number, action } = request;
        sendMessage(number, !_isEmpty(data) ? formatText(data) : 'Game not found!');
        requests.delete(id);
    }

    res.json(true).end();
});

const PORT = process.env.PORT || 1337;
http.createServer(app).listen(PORT, () => {
    console.log(`Express server listening on port ${PORT}`);
});
