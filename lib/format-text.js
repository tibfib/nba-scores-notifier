const _ = require('lodash');

module.exports = function formatText(game) {
    let date = new Date(_.get(game, 'GAME_DATE_EST')).toLocaleDateString('en-us', {
        month: 'short',
        day: 'numeric'
    });

    let away_team = _.trim(_.get(game, 'AWAY.TEAM_ABBREVIATION'));
    let away_score = _.trim(_.get(game, 'AWAY.PTS'));

    let home_team = _.trim(_.get(game, 'HOME.TEAM_ABBREVIATION'));
    let home_score = _.trim(_.get(game, 'HOME.PTS'));

    let game_status = _.trim(_.get(game, 'GAME_STATUS_TEXT'));
    let game_time = _.trim(_.get(game, 'LIVE_PC_TIME'));

    return `${away_team} ${away_score} @ ${home_team} ${home_score} | ${game_status} ${game_time} (${date})`;
};
